/* Generated By:JavaCC: Do not edit this line. parserCadenaConstants.java */
package analizadores.analizarCadena;


/**
 * Token literal values and constants.
 * Generated by org.javacc.parser.OtherFilesGen#start()
 */
public interface parserCadenaConstants {

  /** End of File. */
  int EOF = 0;
  /** RegularExpression Id. */
  int ID = 4;
  /** RegularExpression Id. */
  int NUM = 5;
  /** RegularExpression Id. */
  int NUMERAL = 6;
  /** RegularExpression Id. */
  int CORCHETEA = 7;
  /** RegularExpression Id. */
  int CORCHETEC = 8;
  /** RegularExpression Id. */
  int PUNTO = 9;
  /** RegularExpression Id. */
  int ARROBA = 10;
  /** RegularExpression Id. */
  int POSICION = 11;
  /** RegularExpression Id. */
  int LETTER = 12;
  /** RegularExpression Id. */
  int DIGIT = 13;
  /** RegularExpression Id. */
  int CADENACOMILLAS = 14;
  /** RegularExpression Id. */
  int OTRO = 15;

  /** Lexical state. */
  int DEFAULT = 0;

  /** Literal token values. */
  String[] tokenImage = {
    "<EOF>",
    "\" \"",
    "\"\\r\"",
    "\"\\t\"",
    "<ID>",
    "<NUM>",
    "\"#\"",
    "\"[\"",
    "\"]\"",
    "\".\"",
    "\"@\"",
    "\"posicion(..)\"",
    "<LETTER>",
    "<DIGIT>",
    "<CADENACOMILLAS>",
    "<OTRO>",
  };

}
