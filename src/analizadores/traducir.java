/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analizadores;

import arbol.pregunta;
import arbol.configuracion;
import arbol.opcion;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Kenia
 */
public class traducir {

    int contador = 0;
    int x = 0;
    int y = 0;
    String xForm = "";
    String generalForm = "";
    String var = "";
    String celdaTexto = "";
    Matcher buscar;
    pregunta padrePrincipal;
    Map<String, String> tablaSimbolos = new HashMap<>();
    Map<String, pregunta> elementos = new HashMap<>();
    Map<String, pregunta> preguntas = new HashMap<>();
    Map<String, pregunta> grupos = new HashMap<>();
    Map<String, pregunta> ciclos = new HashMap<>();
    Map<String, opcion> opciones = new HashMap<>();
    Map<String, configuracion> configuraciones = new HashMap<>();
    List<String> erroresSemanticos = new ArrayList<String>();
    Pattern expr = Pattern.compile("(var.xform.)([a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+)(.var.xform)");
    Pattern exprCadMax = Pattern.compile("(cad_max=)([0-9]+)");
    Pattern exprCadMin = Pattern.compile("(cad_min=)([0-9]+)");
    Pattern exprCadFila = Pattern.compile("(cad_fila=)([0-9]+)");
    Pattern exprVF = Pattern.compile("(opcion=)([a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+)");
    Pattern exprInicio = Pattern.compile("(iniciar=)([0-9]+)");
    Pattern exprFin = Pattern.compile("(finalizar=)([0-9]+)");
    Pattern exprFichero = Pattern.compile("(fichero)([a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+)");
    Pattern exprSelUno = Pattern.compile("(selecciona_uno)([a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+)");
    Pattern exprSelMul = Pattern.compile("(selecciona_multiples)([a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+)");

    public String iniciar(nodoArbol raiz) {

        String nom = raiz.produccion.trim();

        switch (nom) {
            case "WORKBOOK":
                recorrerWorkbook1(raiz.hijos);
                break;
        }

        switch (nom) {
            case "WORKBOOK":
                recorrerWorkbook(raiz.hijos);
                break;
        }

        generarXForm();
        System.out.println("*************Errores semanticos*************\n " + Arrays.toString(erroresSemanticos.toArray())
        );
        return xForm;
    }

    public void generarXForm() {
        xForm += "clase XForm {\n\n";
        generarPreguntas();
        generarGruposyCiclos();

        xForm += "formulario Principal {\n";

        for (Map.Entry<String, pregunta> entry : preguntas.entrySet()) {
            String key = entry.getKey();
            pregunta pregunta = entry.getValue();

            if (pregunta.getIdPadreCercano() == null) {
                xForm += pregunta.getCodigoParaQuePapaMeLlame();
            }
        }

        for (Map.Entry<String, pregunta> entry : grupos.entrySet()) {
            String key = entry.getKey();
            pregunta grupo = entry.getValue();

            if (grupo.getIdPadreCercano() == null) {
                xForm += grupo.getCodigoParaQuePapaMeLlame();
            }
        }

        for (Map.Entry<String, pregunta> entry : ciclos.entrySet()) {
            String key = entry.getKey();
            pregunta ciclos = entry.getValue();

            if (ciclos.getIdPadreCercano() == null) {
                xForm += ciclos.getCodigoParaQuePapaMeLlame();
            }
        }

        xForm += "}";

        xForm += "\n}";
    }

    public void generarPreguntas() {

        String contenidoPadre = "";
        String contenidoPregunta = "";
        String contenidoRespuesta = "";
        String clasePregunta = "";
        String llamarPregunta = "";
        String metodoRespuesta = "";
        String metodoCalculo = "";
        for (Map.Entry<String, pregunta> entry : preguntas.entrySet()) {
            metodoRespuesta = "";
            contenidoPregunta = "";
            contenidoRespuesta = "";
            metodoCalculo = "";
            contenidoPadre = "";
            llamarPregunta = "";
            String key = entry.getKey();
            pregunta preg = entry.getValue();

            //************ETIQUETA*****************
            String contenido = "";
            if (preg.getEtiqueta() != null) {
                contenido = preg.getEtiqueta();
                leerVarialblesyParametros(contenido, preg);
                contenido = contenido.replace("var.xform.", "\"+").replace(".var.xform", "+\"");
                contenido = contenido.replace("punto@reemplazar.xform", preg.getIdpregunta());
                contenido = contenido.replace("posicion@reemplazar.xform", "posicion(" + preg.getIdPadreCercano() + ")");
                contenidoPregunta += "   cadena etiqueta = \"" + contenido + "\";\n";
            }

            //************SUGERIR*****************
            if (preg.getSugerir() != null) {
                contenido = preg.getSugerir();
                leerVarialblesyParametros(contenido, preg);
                contenido = contenido.replace("var.xform.", "\"+").replace(".var.xform", "+\"");
                contenido = contenido.replace("punto@reemplazar.xform", preg.getIdpregunta());
                contenido = contenido.replace("posicion@reemplazar.xform", "posicion(" + preg.getIdPadreCercano() + ")");
                contenidoPregunta += "   cadena sugerir = \"" + contenido + "\";\n";
            }

            //************RESTRINGIR*****************
            if (preg.getRestringir() != null) {
                contenido = preg.getRestringir();
                leerVarialblesyParametros(contenido, preg);
                contenido = contenido.replace("var.xform.", "").replace(".var.xform", "");
                contenido = contenido.replace("punto@reemplazar.xform", preg.getIdpregunta());
                contenido = contenido.replace("posicion@reemplazar.xform", "posicion(" + preg.getIdPadreCercano() + ")");
                contenidoRespuesta += "   si (" + contenido + ") {\n"
                        + "   respuesta = param_1;\n"
                        + "   }\n";

                //************RESTRINGIRMSN*****************
                if (preg.getRestringirmsn() != null) {
                    contenido = preg.getRestringirmsn();
                    leerVarialblesyParametros(contenido, preg);
                    contenido = contenido.replace("var.xform.", "").replace(".var.xform", "");
                    contenido = contenido.replace("idPregunta@reemplazar.xform", preg.getIdpregunta());
                    contenido = contenido.replace("posicion@reemplazar.xform", preg.getIdPadreCercano());
                    contenidoRespuesta += "   sino {\n"
                            + "   mensaje (\"" + contenido + "\");\n"
                            + "   }";
                }

            } else {
                contenidoRespuesta = "   respuesta = param_1;\n";

            }

            //************REQUERIR*****************
            if (preg.getRequerido() != null) {
                contenido = preg.getRequerido();
                leerVarialblesyParametros(contenido, preg);
                contenido = contenido.replace("var.xform.", "").replace(".var.xform", "");
                contenido = contenido.replace("punto@reemplazar.xform", preg.getIdpregunta());
                contenido = contenido.replace("posicion@reemplazar.xform", "posicion(" + preg.getIdPadreCercano() + ")");
                contenidoPregunta += "   booleano requerido = \"" + contenido + "\";\n";
            }

            //************REQUERIRMSN*****************
            if (preg.getRequeridomsn() != null) {
                contenido = preg.getRequeridomsn();
                leerVarialblesyParametros(contenido, preg);
                contenido = contenido.replace("var.xform.", "\"+").replace(".var.xform", "+\"");
                contenido = contenido.replace("punto@reemplazar.xform", preg.getIdpregunta());
                contenido = contenido.replace("posicion@reemplazar.xform", "posicion(" + preg.getIdPadreCercano() + ")");
                contenidoPregunta += "   cadena requeridomsn = \"" + contenido + "\";\n";
            }

            //************LECTURA*****************
            if (preg.getLectura() != null) {
                contenido = preg.getLectura();
                leerVarialblesyParametros(contenido, preg);
                contenido = contenido.replace("var.xform.", "").replace(".var.xform", "");
                contenido = contenido.replace("punto@reemplazar.xform", preg.getIdpregunta());
                contenido = contenido.replace("posicion@reemplazar.xform", "posicion(" + preg.getIdPadreCercano() + ")");
                contenidoPregunta += "   booleano lectura = \"" + contenido + "\";\n";
            }

            //************PREDETERMINADO*****************
            if (preg.getPredeterminado() != null) {
                contenido = preg.getPredeterminado();
                leerVarialblesyParametros(contenido, preg);
                contenido = contenido.replace("var.xform.", "").replace(".var.xform", "");
                contenido = contenido.replace("punto@reemplazar.xform", preg.getIdpregunta());
                contenido = contenido.replace("posicion@reemplazar.xform", "posicion(" + preg.getIdPadreCercano() + ")");
                contenidoPregunta += "   " + tablaSimbolos.get(preg.getIdpregunta()) + " respuesta = \"" + contenido + "\";\n";
            } else {
                contenidoPregunta += "   " + tablaSimbolos.get(preg.getIdpregunta()) + " respuesta;\n";
            }

//PEN       //************CALCULO*****************
            String paramCalculo = "";
            if (preg.getCalculo() != null) {
                contenido = preg.getCalculo();
                buscar = expr.matcher(contenido);
                while (buscar.find()) {
                    var = buscar.group(2);
                    if (tablaSimbolos.containsKey(var)) {
                        paramCalculo += tablaSimbolos.get(key) + " " + var + ",";
                    } else {
                        erroresSemanticos.add("Error en la pregunta: " + preg.getIdpregunta() + " la referencia a la pregunta: " + var + " no existe");
                    }
                }
                if (!paramCalculo.isEmpty()) {
                    paramCalculo = paramCalculo.substring(0, paramCalculo.length() - 1);
                }

                contenido = contenido.replace("var.xform.", "").replace(".var.xform", "");
                contenido = contenido.replace("punto@reemplazar.xform", preg.getIdpregunta());
                contenido = contenido.replace("posicion@reemplazar.xform", "posicion(" + preg.getIdPadreCercano() + ")");
                metodoCalculo += "   publico calcular (" + paramCalculo + ") {\n"
                        + "   respuesta = " + contenido + ";\n"
                        + "   }\n";

            }

            //************PARAMETRO***************** 
            String tipo = preg.getTipo().toLowerCase().replaceAll(" ", "");

            if (tipo.contains("texto")) {
                String cadMin = "0";
                String cadMax = "0";
                String cadFila = "0";
                if (preg.getParametro() != null) {
                    contenido = preg.getParametro();
                    leerVarialbles(contenido, preg);
                    contenido = contenido.replace("var.xform.", "").replace(".var.xform", "");
                    contenido = contenido.replace("punto@reemplazar.xform", preg.getIdpregunta());
                    contenido = contenido.replace("posicion@reemplazar.xform", "posicion(" + preg.getIdPadreCercano() + ")");
                    String par = contenido.toLowerCase().replaceAll(" ", "");
                    buscar = exprCadMin.matcher(par);
                    while (buscar.find()) {
                        cadMin = var = buscar.group(2);
                    }
                    buscar = exprCadMax.matcher(par);
                    while (buscar.find()) {
                        cadMax = var = buscar.group(2);
                    }
                    buscar = exprCadFila.matcher(par);
                    while (buscar.find()) {
                        cadFila = var = buscar.group(2);
                    }
                    llamarPregunta = "   " + preg.getIdpregunta() + "( ).respuesta(resp.esCadena).cadena(" + cadMin + "," + cadMax + "," + cadFila + ");\n";
                } else {
                    llamarPregunta = "   " + preg.getIdpregunta() + "( ).respuesta(resp.esCadena).cadena();\n";
                }
            } else if (tipo.contains("rango")) {
                String iniciar = "0";
                String finalizar = "0";
                if (preg.getParametro() != null) {
                    contenido = preg.getParametro();
                    leerVarialbles(contenido, preg);
                    contenido = contenido.replace("var.xform.", "").replace(".var.xform", "");
                    contenido = contenido.replace("punto@reemplazar.xform", preg.getIdpregunta());
                    contenido = contenido.replace("posicion@reemplazar.xform", "posicion(" + preg.getIdPadreCercano() + ")");
                    String par = contenido.toLowerCase().replaceAll(" ", "");
                    buscar = exprInicio.matcher(par);
                    while (buscar.find()) {
                        iniciar = var = buscar.group(2);
                    }
                    buscar = exprFin.matcher(par);
                    while (buscar.find()) {
                        finalizar = var = buscar.group(2);
                    }

                    llamarPregunta = "   " + preg.getIdpregunta() + "( ).respuesta(resp.esEntero).rango(" + iniciar + "," + finalizar + ");\n";
                } else {
                    llamarPregunta = "   " + preg.getIdpregunta() + "( ).respuesta(resp.esEntero).rango();\n";
                }
            } else if (tipo.contains("condicion")) {
                if (preg.getParametro() != null) {
                    contenido = preg.getParametro();
                    leerVarialbles(contenido, preg);
                    contenido = contenido.replace("var.xform.", "").replace(".var.xform", "");
                    contenido = contenido.replace("punto@reemplazar.xform", preg.getIdpregunta());
                    contenido = contenido.replace("posicion@reemplazar.xform", "posicion(" + preg.getIdPadreCercano() + ")");
                    String par = contenido.toLowerCase().replaceAll(" ", "");

                    if (par.contains("v_f") || par.contains("v")) {
                        llamarPregunta = "   " + preg.getIdpregunta() + "( ).respuesta(resp.esBooleano).condicion(Verdadero, Falso);\n";

                    } else {
                        llamarPregunta = "   " + preg.getIdpregunta() + "( ).respuesta(resp.esBooleano).condicion(0, 1);\n";

                    }
                } else {
                    llamarPregunta = "   " + preg.getIdpregunta() + "( ).respuesta(resp.esBooleano).condicion();\n";
                }
            } else if (tipo.contains("entero")) {
                llamarPregunta = "   " + preg.getIdpregunta() + "( ).Respuesta(res.esEntero).entero( );\n";
            } else if (tipo.contains("decimal")) {
                llamarPregunta = "   " + preg.getIdpregunta() + "( ).Respuesta(res.esDecimal).decimal( );\n";
            } else if (tipo.contains("fecha")) {
                llamarPregunta = "   " + preg.getIdpregunta() + "( ).Respuesta(res.esFecha).fecha( );\n";
            } else if (tipo.contains("fecha_hora")) {
                llamarPregunta = "   " + preg.getIdpregunta() + "( ).Respuesta(res.esFecha).fechahora( );\n";
            } else if (tipo.contains("hora")) {
                llamarPregunta = "   " + preg.getIdpregunta() + "( ).Respuesta(res.esFecha).hora( );\n";
            } else if (tipo.contains("seleccion_uno")) {
                String sel = "";
                String par = "   " + preg.getTipo().toLowerCase().replaceAll(" ", "");
                buscar = exprSelUno.matcher(par);
                while (buscar.find()) {
                    sel = var = buscar.group(2);
                }
                llamarPregunta = "   " + preg.getIdpregunta() + "Respuesta(res.esCadena).Seleccionar_1(" + sel + ");\n";
            } else if (tipo.contains("seleccion_multiples")) {
                String sel = "";
                String par = "   " + preg.getTipo().toLowerCase().replaceAll(" ", "");
                buscar = exprSelMul.matcher(par);
                while (buscar.find()) {
                    sel = var = buscar.group(2);
                }
                llamarPregunta = "   " + preg.getIdpregunta() + "Respuesta(res.esCadena).Seleccionar(" + sel + ");\n";
            } else if (tipo.contains("nota")) {
                llamarPregunta = "   " + preg.getIdpregunta() + "( ).nota( );\n";
            } else if (tipo.contains("fichero")) {
                String fich = "";
                String par = "   " + preg.getTipo().toLowerCase().replaceAll(" ", "");
                buscar = exprFichero.matcher(par);
                while (buscar.find()) {
                    fich = var = buscar.group(2);
                }
                llamarPregunta = "   " + preg.getIdpregunta() + "( ).fichero(" + fich + ");\n";
            } else if (tipo.contains("calcular")) {
                llamarPregunta = "   " + preg.getIdpregunta() + "( ).calcular();\n";
            }

            //************APLICABLE*****************
            if (preg.getAplicable() != null) {
                contenido = preg.getAplicable();
                leerVarialbles(contenido, preg);
                contenido = contenido.replace("var.xform.", (preg.getIdPadreGrupal() != null) ? preg.getIdPadreGrupal() + "()." : "").replace(".var.xform", "().respuesta");
                contenido = contenido.replace("idPregunta@reemplazar.xform", preg.getIdpregunta() + ".respuesta()");
                contenido = contenido.replace("posicion@reemplazar.xform", "posicion(" + preg.getIdPadreCercano() + ")");
                contenidoPadre += "   Si (" + contenido + ") {\n"
                        + "   " + llamarPregunta + "\n"
                        + "   }\n";

                preg.setCodigoParaQuePapaMeLlame(contenidoPadre);
            } else {
                contenidoPadre += llamarPregunta;
                preg.setCodigoParaQuePapaMeLlame(contenidoPadre);
            }

            String params = "";
            for (String id : preg.getParametros()) {
                params += tablaSimbolos.get(id) + " " + id + ",";

            }
            /* int cont = 0;
            String escribir = "";
            for (String id : preg.getParametros()) {
                cont = cont + 1;
                if (preg.getParametros().size() == 1) {
                    params += tablaSimbolos.get(id) + " " + id;
                } else if (cont == preg.getParametros().size()) {
                    params += tablaSimbolos.get(id) + " " + id;
                } else {
                    params += tablaSimbolos.get(id) + " " + id + ",";
                }
            }
             */
            if (!params.isEmpty()) {
                params = params.substring(0, params.length() - 1);
            }

            metodoRespuesta = "\n    publico respuesta (" + tablaSimbolos.get(preg.getIdpregunta()) + "   param_1) {\n"
                    + contenidoRespuesta + "   }\n";
            clasePregunta += "Pregunta " + key + "(" + params + "){\n";
            clasePregunta += contenidoPregunta;
            clasePregunta += metodoCalculo;
            clasePregunta += metodoRespuesta;
            clasePregunta += "}\n\n";
        }

        xForm += clasePregunta;
    }

    public void generarGruposyCiclos() {

        String claseGrupo = "";
        String contenidoGrupo = "";
        for (Map.Entry<String, pregunta> entry : grupos.entrySet()) {
            String key = entry.getKey();
            pregunta grupo = entry.getValue();
            String contenido = "";
            String contenidoPadre = "";
            String llamarGrupo = "";
            llamarGrupo = "   " + key + "();\n";
            //************APLICABLE*****************
            if (grupo.getAplicable() != null) {
                contenido = grupo.getAplicable();
                // contenido = contenido.replace("var.xform.", (grupo.getIdPadreGrupal() != null) ? grupo.getIdPadreGrupal() + "()." : "").replace(".var.xform", "().respuesta");
                contenido = contenido.replace("var.xform.", (grupo.getIdPadreGrupal() != null) ? grupo.getIdPadreGrupal() + "()." : "").replace(".var.xform", "().respuesta");
                contenido = contenido.replace("idPregunta@reemplazar.xform", grupo.getIdpregunta() + ".respuesta()");
                contenido = contenido.replace("posicion@reemplazar.xform", "posicion(" + grupo.getIdPadreCercano() + ")");
                contenidoPadre = "   Si (" + contenido + ") {\n"
                        + "   " + llamarGrupo + "\n"
                        + "   }\n";

                grupo.setCodigoParaQuePapaMeLlame(contenidoPadre);
            } else {
                contenidoPadre = llamarGrupo;
                grupo.setCodigoParaQuePapaMeLlame(contenidoPadre);
            }

        }

        for (Map.Entry<String, pregunta> entry : ciclos.entrySet()) {
            String key = entry.getKey();
            pregunta ciclo = entry.getValue();
            String contenido = "";
            String contenidoPadre = "";
            String llamarCiclo = "   " + key + "();\n";
            //************APLICABLE*****************
            if (ciclo.getAplicable() != null) {
                contenido = ciclo.getAplicable();
                // contenido = contenido.replace("var.xform.", (grupo.getIdPadreGrupal() != null) ? grupo.getIdPadreGrupal() + "()." : "").replace(".var.xform", "().respuesta");
                contenido = contenido.replace("var.xform.", (ciclo.getIdPadreGrupal() != null) ? ciclo.getIdPadreGrupal() + "()." : "").replace(".var.xform", "().respuesta");
                contenido = contenido.replace("idPregunta@reemplazar.xform", ciclo.getIdpregunta() + ".respuesta()");
                contenido = contenido.replace("posicion@reemplazar.xform", "posicion(" + ciclo.getIdPadreCercano() + ")");
                contenidoPadre = "   Si (" + contenido + ") {\n"
                        + "   " + llamarCiclo
                        + "   }\n";

            } else {
                contenidoPadre = llamarCiclo;
            }

            //************REPETIR*****************
            if (ciclo.getRepeticion() != null) {
                contenido = ciclo.getRepeticion();
                // contenido = contenido.replace("var.xform.", (grupo.getIdPadreGrupal() != null) ? grupo.getIdPadreGrupal() + "()." : "").replace(".var.xform", "().respuesta");
                contenido = contenido.replace("var.xform.", (ciclo.getIdPadreGrupal() != null) ? ciclo.getIdPadreGrupal() + "()." : "").replace(".var.xform", "().respuesta");
                contenido = contenido.replace("idPregunta@reemplazar.xform", ciclo.getIdpregunta() + ".respuesta()");
                contenido = contenido.replace("posicion@reemplazar.xform", "posicion(" + ciclo.getIdPadreCercano() + ")");
                contenidoPadre = "   Entero " + key + "=0;\n";
                contenidoPadre += "   Para (entero " + key + " = 0; " + key + " <= " + contenido + "; " + key + " ++){\n"
                        + "   " + contenidoPadre
                        + "   }\n";

                ciclo.setCodigoParaQuePapaMeLlame(contenidoPadre);
            } else {
                contenidoPadre = "   Entero " + key + "=0;\n";
                contenidoPadre += "   Para (entero " + key + " = 0; verdadero; " + key + " ++){\n"
                        + "   " + contenidoPadre
                        + "   }\n";
                ciclo.setCodigoParaQuePapaMeLlame(contenidoPadre);
            }

        }

        for (Map.Entry<String, pregunta> entry : grupos.entrySet()) {
            String key = entry.getKey();
            pregunta grupo = entry.getValue();
            contenidoGrupo = "";
            for (String id : grupo.getIdPreguntas()) {
                contenidoGrupo += preguntas.get(id).getCodigoParaQuePapaMeLlame();
            }

            for (String id : grupo.getIdgrupos()) {
                contenidoGrupo += grupos.get(id).getCodigoParaQuePapaMeLlame();
            }

            for (String id : grupo.getIdciclos()) {
                contenidoGrupo += ciclos.get(id).getCodigoParaQuePapaMeLlame();
            }

            claseGrupo += "Grupo " + key + " {\n"
                    + "   Respuesta resp;\n"
                    + contenidoGrupo
                    + "}\n\n";
        }

        for (Map.Entry<String, pregunta> entry : ciclos.entrySet()) {
            String key = entry.getKey();
            pregunta grupo = entry.getValue();
            contenidoGrupo = "";
            for (String id : grupo.getIdPreguntas()) {
                contenidoGrupo += preguntas.get(id).getCodigoParaQuePapaMeLlame();
            }

            for (String id : grupo.getIdgrupos()) {
                contenidoGrupo += grupos.get(id).getCodigoParaQuePapaMeLlame();
            }

            for (String id : grupo.getIdciclos()) {
                contenidoGrupo += ciclos.get(id).getCodigoParaQuePapaMeLlame();
            }

            claseGrupo += "Grupo " + key + " {\\\\ciclo\n"
                    + "   Respuesta resp;\n"
                    + contenidoGrupo
                    + "}\n\n";
        }

        xForm += claseGrupo;

    }

    public void leerVarialblesyParametros(String texto, pregunta obj) {
        buscar = expr.matcher(texto);
        while (buscar.find()) {
            var = buscar.group(2);
            if (tablaSimbolos.containsKey(var)) {
                obj.getParametros().add(var);
            } else {
                erroresSemanticos.add("Error en la pregunta: " + obj.getIdpregunta() + " la referencia a la pregunta: " + var + " no existe");
            }
        }
        //texto = texto.replace("idPregunta@reemplazar.xform", obj.getIdpregunta());
        //texto = texto.replace("posicion@reemplazar.xform", obj.getIdPadreCercano());

    }

    public void leerVarialbles(String texto, pregunta obj) {
        buscar = expr.matcher(texto);
        while (buscar.find()) {
            var = buscar.group(2);
            if (tablaSimbolos.containsKey(var)) {
                // obj.getParametros().add(var);
            } else {
                erroresSemanticos.add("Error en la pregunta: " + obj.getIdpregunta() + " la referencia a la pregunta: " + var + " no existe");
            }
        }

        //   texto = texto.replace("idPregunta@reemplazar.xform", obj.getIdpregunta());
        //   texto = texto.replace("posicion@reemplazar.xform", obj.getIdPadreCercano());
    }

    public void recorrerWorkbook(LinkedList<nodoArbol> hijos) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim().toUpperCase();
            switch (produccion) {
                case "WORKBOOK_CONTENIDO":
                    recorrerWorkbook(hijo.hijos);
                    break;
                case "SHEET=ENCUESTA":
                    recorrerEncuesta(hijo.hijos);
                    break;
                case "SHEET=CONFIGURACION":
                    recorrerConfiguraciones(hijo.hijos);
                    break;
                case "SHEET=OPCIONES":
                    recorrerOpciones(hijo.hijos);
                    break;
            }
        }
    }

    public void recorrerEncuesta(LinkedList<nodoArbol> hijos) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim().toUpperCase();
            switch (produccion) {
                case "SHEET_CONTENIDO":
                    recorrerEncuesta(hijo.hijos);
                    break;
                case "GRUPO":
                    pregunta grupo = new pregunta();
                    recorrerGrupo(hijo.hijos, grupo);
                    grupos.put(grupo.getIdpregunta(), grupo);
                    // //tablaSimbolos.put(grupo.getIdpregunta(), "grupo");
                    break;
                case "CICLO":
                    pregunta ciclo = new pregunta();
                    recorrerCiclo(hijo.hijos, ciclo);
                    ciclos.put(ciclo.getIdpregunta(), ciclo);
                    //tablaSimbolos.put(ciclo.getIdpregunta(), "ciclo");
                    break;
                case "PREGUNTA":
                    pregunta preg = new pregunta();
                    recorrerColumnasEncuesta(hijo.hijos.get(0).hijos, preg);
                    preguntas.put(preg.getIdpregunta(), preg);
                    //tablaSimbolos.put(preg.getIdpregunta(), preg.getTipo());
                    break;
            }
        }
    }

    public void recorrerGrupo(LinkedList<nodoArbol> hijos, pregunta obj) {
        for (nodoArbol hijo : hijos) {
            //  Map<String, pregunta> grupo = new HashMap<>();
            String produccion = hijo.produccion.trim().toUpperCase();
            switch (produccion) {
                case "COLUMNAS":
                    recorrerColumnasEncuesta(hijo.hijos, obj);
                    grupos.put(obj.getIdpregunta(), obj);
                    //tablaSimbolos.put(obj.getIdpregunta(), obj.getTipo());
                    break;
                case "PREGUNTA":
                    pregunta preg = new pregunta();
                    preg.setIdPadreGrupal(obj.getIdpregunta());
                    preg.setIdPadreCercano(obj.getIdpregunta());
                    recorrerColumnasEncuesta(hijo.hijos, preg);
                    preguntas.put(preg.getIdpregunta(), preg);
                    //tablaSimbolos.put(preg.getIdpregunta(), preg.getTipo());
                    obj.getIdPreguntas().add(preg.getIdpregunta());
                    // obj.getPreguntas().put(preg.getIdpregunta(), preg);
                    break;
                case "CICLO":
                    pregunta ciclo = new pregunta();
                    ciclo.setIdPadreGrupal(obj.getIdpregunta());
                    ciclo.setIdPadreCercano(obj.getIdpregunta());
                    recorrerCiclo(hijo.hijos, ciclo);
                    ciclos.put(ciclo.getIdpregunta(), ciclo);
                    //tablaSimbolos.put(ciclo.getIdpregunta(), "ciclo");
                    obj.getIdciclos().add(ciclo.getIdpregunta());
                    // obj.getCiclos().put(ciclo.getIdpregunta(), ciclo);
                    break;
                case "GRUPO":
                    pregunta grupo = new pregunta();
                    grupo.setIdPadreGrupal(obj.getIdpregunta());
                    grupo.setIdPadreCercano(obj.getIdpregunta());
                    recorrerGrupo(hijo.hijos, grupo);
                    grupos.put(grupo.getIdpregunta(), grupo);
                    //tablaSimbolos.put(grupo.getIdpregunta(), "grupo");
                    obj.getIdgrupos().add(grupo.getIdpregunta());
                    // obj.getGrupos().put(grupo.getIdpregunta(), grupo);
                    break;
                case "CONTENIDO":
                    recorrerGrupo(hijo.hijos, obj);
                    break;
            }
        }
    }

    public void recorrerCiclo(LinkedList<nodoArbol> hijos, pregunta obj) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim().toUpperCase();
            switch (produccion) {
                case "COLUMNAS":
                    recorrerColumnasEncuesta(hijo.hijos, obj);
                    ciclos.put(obj.getIdpregunta(), obj);
                    //tablaSimbolos.put(obj.getIdpregunta(), obj.getTipo());
                    break;
                case "PREGUNTA":
                    pregunta preg = new pregunta();
                    preg.setIdPadreGrupal(obj.getIdPadreGrupal());
                    preg.setIdPadreCercano(obj.getIdpregunta());
                    recorrerColumnasEncuesta(hijo.hijos, preg);
                    preguntas.put(preg.getIdpregunta(), preg);
                    //tablaSimbolos.put(preg.getIdpregunta(), preg.getTipo());
                    obj.getIdPreguntas().add(preg.getIdpregunta());
                    //   obj.getPreguntas().put(preg.getIdpregunta(), preg);
                    break;
                case "CICLO":
                    pregunta ciclo = new pregunta();
                    ciclo.setIdPadreGrupal(obj.getIdPadreGrupal());
                    ciclo.setIdPadreCercano(obj.getIdpregunta());
                    recorrerCiclo(hijo.hijos, ciclo);
                    ciclos.put(ciclo.getIdpregunta(), ciclo);
                    //tablaSimbolos.put(ciclo.getIdpregunta(), "ciclo");
                    obj.getIdciclos().add(ciclo.getIdpregunta());
                    //obj.getCiclos().put(ciclo.getIdpregunta(), ciclo);
                    break;
                case "GRUPO":
                    pregunta grupo = new pregunta();
                    grupo.setIdPadreGrupal(obj.getIdPadreGrupal());
                    grupo.setIdPadreCercano(obj.getIdpregunta());
                    recorrerGrupo(hijo.hijos, grupo);
                    grupos.put(grupo.getIdpregunta(), grupo);
                    //tablaSimbolos.put(grupo.getIdpregunta(), "grupo");
                    obj.getIdgrupos().add(grupo.getIdpregunta());
                    //obj.getGrupos().put(grupo.getIdpregunta(), grupo);
                    break;
                case "CONTENIDO":
                    recorrerCiclo(hijo.hijos, obj);
                    break;
            }
        }
    }

    public void recorrerColumnasEncuesta(LinkedList<nodoArbol> hijos, pregunta obj) {

        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim();
            switch (produccion) {
                case "COLUMNAS":
                    recorrerColumnasEncuesta(hijo.hijos, obj);
                    break;
                case "<tipo>":
                    if (hijo.hijos.get(0).produccion.equalsIgnoreCase("texto")) {
                        obj.setTipo("cadena");
                    } else {
                        obj.setTipo(hijo.hijos.get(0).produccion);
                    }
                    break;
                case "<idpregunta>":
                    obj.setIdpregunta(hijo.hijos.get(0).produccion);
                    break;
                case "<etiqueta>":
                    obj.setEtiqueta(hijo.hijos.get(0).produccion);
                    break;
                case "<sugerir>":
                    obj.setSugerir(hijo.hijos.get(0).produccion);
                    break;
                case "<sugerencia>":
                    obj.setSugerir(hijo.hijos.get(0).produccion);
                    break;
                case "<codigo_pre>":
                    obj.setCodigo_pre(hijo.hijos.get(0).produccion);
                    break;
                case "<codigo_post>":
                    obj.setCodigo_post(hijo.hijos.get(0).produccion);
                    break;
                case "<restringir>":
                    obj.setRestringir(hijo.hijos.get(0).produccion);
                    break;
                case "<restringirmsn>":
                    obj.setRestringirmsn(hijo.hijos.get(0).produccion);
                    break;
                case "<requerido>":
                    obj.setRequerido(hijo.hijos.get(0).produccion);
                    break;
                case "<requeridomsn>":
                    obj.setRequeridomsn(hijo.hijos.get(0).produccion);
                    break;
                case "<predeterminado>":
                    obj.setPredeterminado(hijo.hijos.get(0).produccion);
                    break;
                case "<pordefecto>":
                    obj.setPredeterminado(hijo.hijos.get(0).produccion);
                    break;
                case "<aplicable>":
                    obj.setAplicable(hijo.hijos.get(0).produccion);
                    break;
                case "<lectura>":
                    obj.setLectura(hijo.hijos.get(0).produccion);
                    break;
                case "<calculo>":
                    obj.setCalculo(hijo.hijos.get(0).produccion);
                    break;
                case "<repeticion>":
                    obj.setRepeticion(hijo.hijos.get(0).produccion);
                    break;
                case "<multimedia>":
                    obj.setMultimedia(hijo.hijos.get(0).produccion);
                    break;
                case "<apariencia>":
                    obj.setApariencia(hijo.hijos.get(0).produccion);
                    break;
                case "<parametro>":
                    obj.setParametro(hijo.hijos.get(0).produccion);
                    break;
            }
        }

    }

    public void recorrerConfiguraciones(LinkedList<nodoArbol> hijos) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim().toUpperCase();
            switch (produccion) {
                case "SHEET_CONTENIDO":
                    recorrerConfiguraciones(hijo.hijos);
                    break;
                case "CONFIGURACION":
                    configuracion conf = new configuracion();
                    recorrerColumnasConfiguracion(hijo.hijos, conf);
                    configuraciones.put(conf.getIdform(), conf);
                    ////tablaSimbolos.put(op.getIdpregunta(), op.getTipo());
                    break;
            }
        }
    }

    public void recorrerColumnasConfiguracion(LinkedList<nodoArbol> hijos, configuracion obj) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim();
            switch (produccion) {
                case "COLUMNAS":
                    recorrerColumnasConfiguracion(hijo.hijos, obj);
                    break;
                case "<titulo_formulario>":
                    obj.setTitulo_formulario(hijo.hijos.get(0).produccion);
                    break;
                case "<idform>":
                    obj.setIdform(hijo.hijos.get(0).produccion);
                    break;
                case "<estilo>":
                    obj.setEstilo(hijo.hijos.get(0).produccion);
                    break;
                case "<importar>":
                    obj.setImportar(hijo.hijos.get(0).produccion);
                    break;
                case "<codigo_principal>":
                    obj.setCodigo_principal(hijo.hijos.get(0).produccion);
                    break;
                case "<codigo_global>":
                    obj.setCodigo_global(hijo.hijos.get(0).produccion);
                    break;
            }
        }
    }

    public void recorrerOpciones(LinkedList<nodoArbol> hijos) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim().toUpperCase();
            switch (produccion) {
                case "SHEET_CONTENIDO":
                    recorrerOpciones(hijo.hijos);
                    break;
                case "OPCION":
                    opcion op = new opcion();
                    recorrerColumnasOpcion(hijo.hijos, op);
                    opciones.put(op.getNombre(), op);
                    ////tablaSimbolos.put(op.getIdpregunta(), op.getTipo());
                    break;
            }
        }
    }

    public void recorrerColumnasOpcion(LinkedList<nodoArbol> hijos, opcion obj) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim();
            switch (produccion) {
                case "COLUMNAS":
                    recorrerColumnasOpcion(hijo.hijos, obj);
                    break;

                case "<multimedia>":
                    obj.setMultimedia(hijo.hijos.get(0).produccion);
                    break;
                case "<etiqueta>":
                    obj.setEtiqueta(hijo.hijos.get(0).produccion);
                    break;
                case "<nombre_lista>":
                    obj.setNombre_lista(hijo.hijos.get(0).produccion);
                    break;
                case "<nombre>":
                    obj.setNombre(hijo.hijos.get(0).produccion);
                    break;
            }
        }
    }

    public void recorrerWorkbook1(LinkedList<nodoArbol> hijos) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim().toUpperCase();
            switch (produccion) {
                case "WORKBOOK_CONTENIDO":
                    recorrerWorkbook1(hijo.hijos);
                    break;
                case "SHEET=ENCUESTA":
                    recorrerEncuesta1(hijo.hijos);
                    break;
                case "SHEET=CONFIGURACION":
                    recorrerConfiguraciones(hijo.hijos);
                    break;
                case "SHEET=OPCIONES":
                    recorrerOpciones(hijo.hijos);
                    break;
            }
        }
    }

    public void recorrerEncuesta1(LinkedList<nodoArbol> hijos) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim().toUpperCase();
            switch (produccion) {
                case "SHEET_CONTENIDO":
                    recorrerEncuesta1(hijo.hijos);
                    break;
                case "GRUPO":
                    pregunta grupo = new pregunta();
                    recorrerGrupo1(hijo.hijos, grupo);
                    tablaSimbolos.put(grupo.getIdpregunta(), "grupo");
                    break;
                case "CICLO":
                    pregunta ciclo = new pregunta();
                    recorrerCiclo1(hijo.hijos, ciclo);
                    tablaSimbolos.put(ciclo.getIdpregunta(), "ciclo");
                    break;
                case "PREGUNTA":
                    pregunta preg = new pregunta();
                    recorrerColumnasEncuesta1(hijo.hijos.get(0).hijos, preg);
                    // tablaSimbolos.put(preg.getIdpregunta(), preg.getTipo());
                    putTablaSim(preg);
                    break;
            }
        }
    }

    public void recorrerGrupo1(LinkedList<nodoArbol> hijos, pregunta obj) {
        for (nodoArbol hijo : hijos) {
            //  Map<String, pregunta> grupo = new HashMap<>();
            String produccion = hijo.produccion.trim().toUpperCase();
            switch (produccion) {
                case "COLUMNAS":
                    recorrerColumnasEncuesta1(hijo.hijos, obj);
                    tablaSimbolos.put(obj.getIdpregunta(), obj.getTipo());
                    break;
                case "PREGUNTA":
                    pregunta preg = new pregunta();
                    recorrerColumnasEncuesta1(hijo.hijos, preg);
                    //tablaSimbolos.put(preg.getIdpregunta(), preg.getTipo());
                    putTablaSim(preg);
                    break;
                case "CICLO":
                    pregunta ciclo = new pregunta();
                    recorrerCiclo1(hijo.hijos, ciclo);
                    tablaSimbolos.put(ciclo.getIdpregunta(), "ciclo");
                    break;
                case "GRUPO":
                    pregunta grupo = new pregunta();
                    recorrerGrupo1(hijo.hijos, grupo);
                    tablaSimbolos.put(grupo.getIdpregunta(), "grupo");
                    break;
                case "CONTENIDO":
                    recorrerGrupo1(hijo.hijos, obj);
                    break;
            }
        }
    }

    public void recorrerCiclo1(LinkedList<nodoArbol> hijos, pregunta obj) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim().toUpperCase();
            switch (produccion) {
                case "COLUMNAS":
                    recorrerColumnasEncuesta1(hijo.hijos, obj);
                    tablaSimbolos.put(obj.getIdpregunta(), obj.getTipo());
                    break;
                case "PREGUNTA":
                    pregunta preg = new pregunta();
                    recorrerColumnasEncuesta1(hijo.hijos, preg);
                    //  tablaSimbolos.put(preg.getIdpregunta(), preg.getTipo());
                    putTablaSim(preg);
                    break;
                case "CICLO":
                    pregunta ciclo = new pregunta();
                    recorrerCiclo1(hijo.hijos, ciclo);
                    tablaSimbolos.put(ciclo.getIdpregunta(), "ciclo");
                    break;
                case "GRUPO":
                    pregunta grupo = new pregunta();
                    recorrerGrupo1(hijo.hijos, grupo);
                    tablaSimbolos.put(grupo.getIdpregunta(), "grupo");
                    break;
                case "CONTENIDO":
                    recorrerCiclo1(hijo.hijos, obj);
                    break;
            }
        }
    }

    public void putTablaSim(pregunta obj) {
        String tipo = obj.getTipo().toLowerCase().replaceAll(" ", "");
        if (tipo.contains("texto")) {
            tablaSimbolos.put(obj.getIdpregunta(), "cadena");
        } else if (tipo.contains("rango")) {
            tablaSimbolos.put(obj.getIdpregunta(), "entero");
        } else if (tipo.contains("condicion")) {
            tablaSimbolos.put(obj.getIdpregunta(), "boolean");
        } else if (tipo.contains("entero")) {
            tablaSimbolos.put(obj.getIdpregunta(), "entero");
        } else if (tipo.contains("decimal")) {
            tablaSimbolos.put(obj.getIdpregunta(), "decimal");
        } else if (tipo.contains("fecha")) {
            tablaSimbolos.put(obj.getIdpregunta(), "fecha");
        } else if (tipo.contains("fecha_hora")) {
            tablaSimbolos.put(obj.getIdpregunta(), "fechahora");
        } else if (tipo.contains("hora")) {
            tablaSimbolos.put(obj.getIdpregunta(), "hora");
        } else if (tipo.contains("selecciona_uno") || tipo.contains("seleccion")) {
            tablaSimbolos.put(obj.getIdpregunta(), "cadena");
        } else if (tipo.contains("selecciona_multiples") || tipo.contains("seleccion")) {
            tablaSimbolos.put(obj.getIdpregunta(), "cadena");
        } else if (tipo.contains("nota")) {
            tablaSimbolos.put(obj.getIdpregunta(), "cadena");
        } else if (tipo.contains("fichero")) {
            tablaSimbolos.put(obj.getIdpregunta(), "fichero");
        } else if (tipo.contains("calcular")) {
            tablaSimbolos.put(obj.getIdpregunta(), "calcular");
        }

    }

    public void recorrerColumnasEncuesta1(LinkedList<nodoArbol> hijos, pregunta obj) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim();
            switch (produccion) {
                case "COLUMNAS":
                    recorrerColumnasEncuesta1(hijo.hijos, obj);
                    break;
                case "<tipo>":

                    obj.setTipo(hijo.hijos.get(0).produccion);

                    break;
                case "<idpregunta>":
                    obj.setIdpregunta(hijo.hijos.get(0).produccion);
                    break;
                case "<calcular>":
                    obj.setIdpregunta(hijo.hijos.get(0).produccion);
                    break;
            }
        }

    }

    public static enum tipoN {
        PREGUNTA,
        GRUPO,
        CICLO;

        public static tipoN getPREGUNTA() {
            return PREGUNTA;
        }

        public static tipoN getGRUPO() {
            return GRUPO;
        }

        public static tipoN getCICLO() {
            return CICLO;
        }
    }
}
