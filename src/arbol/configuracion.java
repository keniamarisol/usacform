/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 * @author Kenia
 */
public class configuracion {

    String titulo_formulario;
    String idform;
    String estilo;
    String importar;
    String codigo_principal;
    String codigo_global;

    public String getTitulo_formulario() {
        return titulo_formulario;
    }

    public void setTitulo_formulario(String titulo_formulario) {
        this.titulo_formulario = titulo_formulario;
    }

    public String getIdform() {
        return idform;
    }

    public void setIdform(String idform) {
        this.idform = idform;
    }

    public String getEstilo() {
        return estilo;
    }

    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }

    public String getImportar() {
        return importar;
    }

    public void setImportar(String importar) {
        this.importar = importar;
    }

    public String getCodigo_principal() {
        return codigo_principal;
    }

    public void setCodigo_principal(String codigo_principal) {
        this.codigo_principal = codigo_principal;
    }

    public String getCodigo_global() {
        return codigo_global;
    }

    public void setCodigo_global(String codigo_global) {
        this.codigo_global = codigo_global;
    }
    
    

}
