/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Kenia
 */
public class pregunta {

    public enum tipoN {
        PREGUNTA,
        GRUPO,
        CICLO;


    }
    /**
     * Tipo del nodo.
     */
    tipoN tipoNodo;
    String tipo;
    String idPadreGrupal;
    String idPadreCercano;
    String codigoParaQuePapaMeLlame;
    String idpregunta;
    String etiqueta;
    String sugerir;
    String sugerencia;
    String codigo_pre;
    String codigo_post;
    String restringir;
    String restringirmsn;
    String requerido;
    String requeridomsn;
    String predeterminado;
    String pordefecto;
    String aplicable;
    String lectura;
    String calculo;
    String repeticion;
    String multimedia;
    String apariencia;
    String parametro;
   /* Map<String, pregunta> preguntas = new HashMap<>();
    Map<String, pregunta> grupos = new HashMap<>();
    Map<String, pregunta> ciclos = new HashMap<>();*/
    List<String> idPreguntas = new ArrayList<>();
    List<String> idgrupos = new ArrayList<>();
    List<String> idciclos = new ArrayList<>();
    List<String> parametros = new ArrayList<>();

    public tipoN getTipoNodo() {
        return tipoNodo;
    }

    public void setTipoNodo(tipoN tipoNodo) {
        this.tipoNodo = tipoNodo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getIdpregunta() {
        return idpregunta;
    }

    public void setIdpregunta(String idpregunta) {
        this.idpregunta = idpregunta;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public String getSugerir() {
        return sugerir;
    }

    public void setSugerir(String sugerir) {
        this.sugerir = sugerir;
    }

    public String getSugerencia() {
        return sugerencia;
    }

    public void setSugerencia(String sugerencia) {
        this.sugerencia = sugerencia;
    }

    public String getCodigo_pre() {
        return codigo_pre;
    }

    public void setCodigo_pre(String codigo_pre) {
        this.codigo_pre = codigo_pre;
    }

    public String getCodigo_post() {
        return codigo_post;
    }

    public void setCodigo_post(String codigo_post) {
        this.codigo_post = codigo_post;
    }

    public String getRestringir() {
        return restringir;
    }

    public void setRestringir(String restringir) {
        this.restringir = restringir;
    }

    public String getRestringirmsn() {
        return restringirmsn;
    }

    public void setRestringirmsn(String restringirmsn) {
        this.restringirmsn = restringirmsn;
    }

    public String getRequerido() {
        return requerido;
    }

    public void setRequerido(String requerido) {
        this.requerido = requerido;
    }

    public String getRequeridomsn() {
        return requeridomsn;
    }

    public void setRequeridomsn(String requeridomsn) {
        this.requeridomsn = requeridomsn;
    }

    public String getPredeterminado() {
        return predeterminado;
    }

    public void setPredeterminado(String predeterminado) {
        this.predeterminado = predeterminado;
    }

    public String getPordefecto() {
        return pordefecto;
    }

    public void setPordefecto(String pordefecto) {
        this.pordefecto = pordefecto;
    }

    public String getAplicable() {
        return aplicable;
    }

    public void setAplicable(String aplicable) {
        this.aplicable = aplicable;
    }

    public String getLectura() {
        return lectura;
    }

    public void setLectura(String lectura) {
        this.lectura = lectura;
    }

    public String getCalculo() {
        return calculo;
    }

    public void setCalculo(String calculo) {
        this.calculo = calculo;
    }

    public String getRepeticion() {
        return repeticion;
    }

    public void setRepeticion(String repeticion) {
        this.repeticion = repeticion;
    }

    public String getMultimedia() {
        return multimedia;
    }

    public void setMultimedia(String multimedia) {
        this.multimedia = multimedia;
    }

    public String getApariencia() {
        return apariencia;
    }

    public void setApariencia(String apariencia) {
        this.apariencia = apariencia;
    }

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }


    public List<String> getIdPreguntas() {
        return idPreguntas;
    }

    public void setIdPreguntas(List<String> idPreguntas) {
        this.idPreguntas = idPreguntas;
    }

    public List<String> getIdgrupos() {
        return idgrupos;
    }

    public void setIdgrupos(List<String> idgrupos) {
        this.idgrupos = idgrupos;
    }

    public List<String> getIdciclos() {
        return idciclos;
    }

    public void setIdciclos(List<String> idciclos) {
        this.idciclos = idciclos;
    }

    public List<String> getParametros() {
        return parametros;
    }

    public void setParametros(List<String> parametros) {
        this.parametros = parametros;
    }

    public String getIdPadreGrupal() {
        return idPadreGrupal;
    }

    public void setIdPadreGrupal(String idPadreGrupal) {
        this.idPadreGrupal = idPadreGrupal;
    }

    public String getIdPadreCercano() {
        return idPadreCercano;
    }

    public void setIdPadreCercano(String idPadreCercano) {
        this.idPadreCercano = idPadreCercano;
    }

    public String getCodigoParaQuePapaMeLlame() {
        return codigoParaQuePapaMeLlame;
    }

    public void setCodigoParaQuePapaMeLlame(String codigoParaQuePapaMeLlame) {
        this.codigoParaQuePapaMeLlame = codigoParaQuePapaMeLlame;
    }
    
    

}
